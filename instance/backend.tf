terraform {
  backend "gcs" {
    bucket      = "terraform-remote-state-ta"
    prefix      = "circleci/"
    credentials = "credentials.json"
  }
}