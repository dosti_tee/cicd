data "google_compute_subnetwork" "default" {
  name = "default"
}

# Create a compute engine instance
resource "google_compute_instance" "vm-instance" {
  count        = var.vm_cond ? var.vm_count : 0
  name         = "test-vm-${count.index}"
  zone         = var.zone
  machine_type = "f1-micro"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
      size  = "50"
    }
  }
  can_ip_forward = false
  network_interface {
    subnetwork = data.google_compute_subnetwork.default.self_link
    access_config {
      # Ephemeral IP for the instance
    }
  }
}