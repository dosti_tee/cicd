variable "project_id" {
  type = string
}

variable "region" {
  type    = string
  default = "us-central1"
}

variable "zone" {
  type    = string
  default = "us-central1-a"
}

variable "vm_cond" {
  type        = bool
  default     = true
  description = "Condition for vm resources to be created or not"
}

variable "vm_count" {
  type        = number
  description = "Count of resources to be created"
}
