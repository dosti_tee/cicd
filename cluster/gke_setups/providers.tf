provider "google" {
  project     = var.project_id
  zone        = var.zone
  credentials = "../.gcp/credentials.json"
}

provider "random" {
}