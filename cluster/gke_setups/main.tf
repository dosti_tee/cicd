# GKE cluster
resource "google_container_cluster" "primary" {
  count    = 1

  name     = "${var.project_id}-gke-${var.zone}"
  location = var.zone

  initial_node_count       = 1
  remove_default_node_pool = true

  network    = "default"
  subnetwork = "default"

  master_auth {
    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_node_pool" {
  name       = "${var.project_id}-gke-node-pool"
  location   = google_container_cluster.primary[0].location
  cluster    = google_container_cluster.primary[0].name
  node_count = var.min_num_nodes

  node_config {
    image_type   = var.image_type
    machine_type = var.machine_type

    # service_account = ""
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
    labels = {
      env = var.project_id
    }
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }

  autoscaling {
    min_node_count = var.min_num_nodes
    max_node_count = var.max_num_nodes
  }
  depends_on = [
    google_container_cluster.primary,
  ]
}