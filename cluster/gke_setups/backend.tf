terraform {
  backend "gcs" {
    bucket      = "terraform-remote-state-ta"
    prefix      = "circleci/state/gke"
    credentials = "../.gcp/credentials.json"
  }
}