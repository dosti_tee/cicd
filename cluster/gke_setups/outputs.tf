output "project_id" {
  value       = var.project_id
  description = "GCloud Project ID"
}

output "zone" {
  value       = var.zone
  description = "GCloud Zone"
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.primary[0].name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = google_container_cluster.primary[0].endpoint
  description = "GKE Cluster Host"
}

output "ingress_ip_name" {
  value       = google_compute_global_address.external-static-ip.name
  description = "Reserved ip address name for external endpoint"
}

output "ingress_host" {
  value       = trimsuffix(google_dns_record_set.dns-record.name, ".")
  description = "Ingress host"
}

output "ingress_ip_address" {
  value       = google_compute_global_address.external-static-ip.address
  description = "Reserve ip address for external endpoint"
}