variable "project_id" {
  type = string
}

variable "zone" {
  type = string
}

variable "min_num_nodes" {
  type    = number
  default = 1
}

variable "max_num_nodes" {
  type    = number
  default = 3
}

variable "image_type" {
  type    = string
  default = "COS_CONTAINERD"
}

variable "machine_type" {
  type    = string
  default = "n1-standard-2"
}

variable "address_name" {
  type    = string
  default = "nginx-static-ip"
}

variable "dns_zone_name" {
  type    = string
  default = "default-zone"
}

variable "domain_name" {
  type = string
}

variable "dns_record_name" {
  type = string
}
