resource "google_compute_global_address" "external-static-ip" {
  name = var.address_name
}

resource "google_dns_managed_zone" "dns-zone" {
  count      = 1
  name       = var.dns_zone_name
  dns_name   = var.domain_name
  visibility = "public"
}

resource "google_dns_record_set" "dns-record" {
  managed_zone = google_dns_managed_zone.dns-zone[0].name
  name         = "${var.dns_record_name}.${google_dns_managed_zone.dns-zone[0].dns_name}"
  type         = "A"
  ttl          = 300
  rrdatas      = [google_compute_global_address.external-static-ip.address]
}