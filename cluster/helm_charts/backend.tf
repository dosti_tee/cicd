terraform {
  backend "gcs" {
    bucket      = "terraform-remote-state-ta"
    prefix      = "circleci/state/app"
    credentials = "../.gcp/credentials.json"
  }
}