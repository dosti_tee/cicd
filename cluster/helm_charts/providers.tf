# GKE cluster remote state file
data "terraform_remote_state" "gke-cluster" {
  backend = "gcs"

  config = {
    bucket      = "terraform-remote-state-ta"
    prefix      = "circleci/state/gke"
    credentials = "../.gcp/credentials.json"
  }
}

provider "google" {
  project     = data.terraform_remote_state.gke-cluster.outputs.project_id
  zone        = data.terraform_remote_state.gke-cluster.outputs.zone
  credentials = "../.gcp/credentials.json"
}

/* Retrieve GKE cluster information for Kubernetes and Helm providers with Oauth2 access token using google_client_config.
The new token will expire after 1 hour. */
data "google_client_config" "default" {}

data "google_container_cluster" "cluster-europe-west1" {
  name     = data.terraform_remote_state.gke-cluster.outputs.kubernetes_cluster_name
  location = data.terraform_remote_state.gke-cluster.outputs.zone
}

provider "helm" {
  kubernetes {
    host                   = data.terraform_remote_state.gke-cluster.outputs.kubernetes_cluster_host
    token                  = data.google_client_config.default.access_token
    cluster_ca_certificate = base64decode(data.google_container_cluster.cluster-europe-west1.master_auth[0].
    cluster_ca_certificate)
  }
}

provider "kubernetes" {
  host                   = data.terraform_remote_state.gke-cluster.outputs.kubernetes_cluster_host
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.cluster-europe-west1.master_auth[0].cluster_ca_certificate)
}