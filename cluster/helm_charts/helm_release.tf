resource "helm_release" "nginx-microservice" {
  name  = "mydemo"
  chart = "${path.module}/charts/nginx"

  values = [
    file("${path.module}/charts/nginx/values.yaml")
  ]

  set {
    name  = "ingress.ip_address_name"
    value = data.terraform_remote_state.gke-cluster.outputs.ingress_ip_name
  }

  set {
    name  = "ingress.host"
    value = data.terraform_remote_state.gke-cluster.outputs.ingress_host
  }
}