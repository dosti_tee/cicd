# Terraform configuration

## Overview

This repository holds the sample code to provision cloud infrastructure and deploy a microservice application on GKE using Terraform.

A new project may be created for the application (or use default).

Basic requirements for the project are:
Enable all necessary APIs:

- Kubernetes Engine API
- Compute Engine API
- Cloud DNS API
- Cloud Storage API
- Cloud Resource Manager API (for terraform)

### Execution steps

Create a new Service Account for Terraform with neccessary permissions and generate an access key to the project or use your active Application Default Credentials.
Create a Cloud Storage Bucket for Terraform remote state.
Ensure that the Cloud Storage Bucket for Terraform remote state is the same in the backend.tf files in the subdirectories and add the correct bucket name to provider.tf file in the helm_charts directory.

**Service account key file**
Create a folder with name the name _*".gcp"*_ in the root directory with service account credentials placed inside it.
Alternatively, the service account access key could be placed in a path of your choice. In this case, the credentials path defined in backend.tf and provider.tf files inside each subfolder should be replaced with the actual path.

**Main variables to be replaced are:**
|    variables    |             PATH              |
|:---------------:|:-----------------------------:|
| project_id      | ./gke_setups/terraform.tfvars |
| zone            | ./gke_setups/terraform.tfvars |
| dns_zone_name   | ./gke_setups/terraform.tfvars |
| dns_name        | ./gke_setups/terraform.tfvars |
| dns_record_name | ./gke_setups/terraform.tfvars |

To provision the resources, navigate into the "./gke_setups/" directory and run your terraform commands as listed below.

__***Terraform commands***__

- terraform init        # Initializes terraform
- terraform validate    # Validate codes are correct (optional)
- terraform plan        # View the resources that are about to be configured (optional)
- terraform apply       # Provision resources

The microservice application configuration can be found in the "./helm_charts/charts/" directory that holds the helm chart for the application.
Values.yaml file provides the values passed into the chart. Changes to application can be done using this file.

For application deployment, change into the "./helm_charts/" directory and repeat the terraform commands as previously described.
