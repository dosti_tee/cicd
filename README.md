# Circleci with GCP

## Running Terraform in Circleci pipeline

This repository consist of terraform configuration files to provision vms on GCP.

The provisioning of the vms is automated using Circleci whose configuration can be found in the .circleci directory.

**Main variables to be replaced are:**

| variables  |            PATH             |
|:-----------|:----------------------------|
| project_id | ./instance/terraform.tfvars |
| vm_cond    | ./instance/terraform.tfvars |
| vm_count   | ./instance/terraform.tfvars |

_Note_: vm_cond variable is commented out as there is a default value set to true.
